import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private productos: Producto[] = [
    {
      img: "assets/img/aquaman.png",
      nombre: "CAFÉ IRLANDÉS",
      bio: "COMPRAR",
      precio: 170,
    }
  ];


  constructor() { }
}


export interface Producto { //el export hace que lo podamos usar en todo el proyecto
  img: string;
  nombre: string;
  bio: string;
  precio: number;
  //colocamos el valor idx como opcional
  idx?: number;
}
